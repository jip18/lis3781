# LIS3781 Advanced Database Management

## Joshua Petree

### Assignment #3 Requirements:

*2 Parts*

1. Create and populate tables in Oracle server.
2. Run several different commands using the new tables

#### README.md file should include the following items:

* Screenshots of SQL code
* Screenshot of populated tables


#### Assignment Screenshots:

*Screenshot of SQL code (Part 1)*:

![SQL Code Part 1 Screenshot](img/a3_code_p1.PNG)

*Screenshot of SQL code (Part 2)*:

![SQL Code Part 2 Screenshot](img/a3_code_p2.PNG)

*Screenshot of SQL code (Part 3)*:

![SQL Code Part 3 Screenshot](img/a3_code_p3.PNG)

*Screenshot of Populated Tables (Part 1)*:

![Populated Tables Part 1 Screenshot](img/a3_poptable_1.PNG)

*Screenshot of Populated Tables (Part 2)*:

![Populated Tables Part 2 Screenshot](img/a3_poptable_2.PNG)

#### Tutorial Links:

*Log-in to Oracle Tutorial:*
[Log-in to Oracle Tutorial Link](https://youtu.be/Ii6n5aT5AUg "Log-in to Oracle Tutorial")
