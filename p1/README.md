# LIS3781 Advanced Database Management

## Joshua Petree

### Assignment #3 Requirements:

*2 Parts*

1. Create and populate tables in SQL.
2. Run several different commands using the new tables

#### README.md file should include the following items:

* Screenshots of ERD
* Inclusion of SQL code (can be found within the p1 folder)


#### Assignment Screenshots:

*Screenshot of ERD*

![ERD Screenshot](img/lis3781_p1_erd.PNG)