-- Number 1
drop view if exists v_attorney_info;
create view v_attorney_info as

select concat(per_lname, ", ", per_fname) as name,
concat(per_street, ", ", per_city, ", ", per_state, " ", per_zip) as address,
TIMESTAMPDIFF(year, per_dob, now()) as age,
concat('$', format(aty_hourly_rate, 2)) as hourly_rate,
bar_name, spc_type
from person
natural join attorney
natural join bar
natural join specialty
order by per_lname;

--Number 2
drop procedure if exists sp_num_judges_born_by_months;
DELIMITER //
create procedure sp_num_judges_born_by_months()
begin
select month(per_dob) as month, monthname(per_dob) as month_name, count(*) as count
from person
natural join judge
group by month_name
order by month;
end //
DELIMITER ;

call sp_num_judges_born_by_months();

-- Number 3
drop procedure if exists sp_cases_and_judges;
DELIMITER //
create procedure sp_cases_and_judges()
begin

select per_id, cse_id, cse_type, cse_description,
concat(per_fname, " ", per_lname) as name,
concat('(',substring(phn_num, 1, 3), ')', substring(phn_num, 4, 3), '-', substring(phn_num, 7, 4)) as judge_office_num,
phn_type, jud_years_in_practice, cse_start_date, cse_end_date
from person
natural join judge
natural join `case`
natural join phone
where per_type='j'
order_by per_lname;

end //
DELIMITER ;

call sp_cases_and_judges();

-- Number 4
drop trigger if exists trg_judge_history_after_insert;
DELIMITER //
create trigger trg_judge_history_after_insert
after insert on judge
for each row
begin

insert into judge_hist
(per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
values
(
new.per_id, new.crt_id, current_timestamp(), 'i', new.jud_salary,
concat("modifying user: ", user(), " Notes: ", new.jud_notes)
);
end //
DELIMITER ;

--Number 5
drop trigger if exists trg_judge_history_after_insert;
DELIMITER //
create trigger trg_judge_history_after_insert
after update on judge
for each row
begin

insert into judge_hist
(per_id, jhs_crt_id, jhs_date, jhs_type, jhs_salary, jhs_notes)
values
(
new.per_id, new.crt_id, current_timestamp(), 'u', new.jud_salary,
concat("modifying user: ", user(), " Notes: ", new.jud_notes)
);
end //
DELIMITER ;

-- Number 6
drop procedure if exists sp_add_judge_record;
create procedure sp_add_judge_record()
begin
insert into judge
(per_id, crt_id, jud_salary, jud_years_in_practice, jud_notes)
values
(6, 1, 110000, 0, concat("New judge was former attorney. Modifying event creator: ", current_user()));
end //
DELIMITER ;

show variables like 'event_scheduler';
set global event_scheduler = on;

drop event if exists one_time_add_judge;
DELIMITER //
create event if not exists one_time_add_judge
on schedule
at now() + interval 1 hour
comment 'adds a judge record only one time'
do
begin
call sp_add_judge_record();
end //
DELIMITER ;

show events from jip18;
show processlist;