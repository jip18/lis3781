> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS3781 - Advanced Database Management

## Joshua Petree

### LIS3781 Requirements:

*Course Work Links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install AMPPS
    - Provide screenshots of installations
	- Create Bitbucket repo
	- Complete Bitbucket tutorial
	- Provide git command descriptions

2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Provide screenshots of SQL code
	- Provide screenshot of populated tables

3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Provide screenshots of SQL code
	- Provide screenshots of populated tables

4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Provide screenshot of ERD
	- Provide screenshots of SQL code (optional)

5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Provide screenshot of ERD
	- Provide screenshot of SQL code (optional)

6. [P1 README.md](p1/README.md "My P1 README.md file")
    - Provide a screenshot of the ERD
	- Provide the SQL code used for solutions

7. [P2 README.md](p2/README.md "My P2 README.md file")
    - Provide a screenshot of a shell command
