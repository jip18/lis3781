# LIS3781 - Advanced Database Management

## Joshua Petree

### Project 2 Requirements:

*2 Parts:*

1. Install and setup MongoDB and MongoDB tools onto computer
2. Run different shell commands via the command line

#### README.md file should include the following items:

* Screenshot of a shell command used for MongoDB

#### Assignment Screenshots:

*Screenshot of shell command 'show dbs' being executed*:

![P2 Command](img/lis3781_p2_shell_command.PNG)

