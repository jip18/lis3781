> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Joshua Petree

### Assignment #2 Requirements:

*3 Parts:*

1. Create and run SQL code with inserts of Company and Customer tables on both local and remote CCI servers
2. Create users on local server ONLY
3. Grant user permissions on local server
Note: code for second and third part not required to upload for assignment

#### README.md file should include the following items:

* Screenshots of SQL code
* Screenshot of populated tables


#### Assignment Screenshots:

*Screenshot of SQL code (Part 1):

![AMPPS Installation Screenshot](img/a2_sql_p1.PNG)

*Screenshot of SQL code (Part 2):

![AMPPS Installation Screenshot](img/a2_sql_p2.PNG)

*Screenshot of SQL code (Part 3):

![AMPPS Installation Screenshot](img/a2_sql_p3.PNG)

*Screenshot of populated tables:

![AMPPS Installation Screenshot](img/a2_tables.PNG)

#### Tutorial Links:

*QCITR Tutorial - Granting Privileges:*
[Granting Privileges Tutorial Link](http://www.qcitr.com/docs/Granting_Privileges.pdf "Granting Privileges")
