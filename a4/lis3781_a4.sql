set ansi_warnings on;
go

use master;
go

if exists (select name from master.dbo.sysdatabases where name = N'jip18')
drop database jip18;
go

if not exists (select name from master.dbo.sysdatabases where name = N'jip18')
create database jip18;
go

use jip18;
go

if object_id (N'dbo.person', N'U') is not null
drop table dbo.person;
go

create table dbo.person
(
per_id smallint not null identity(1,1),
per_ssn binary(64) null,
per_fname varchar(15) not null,
per_lname varchar(30) not null,
per_gender char(1) not null check (per_gender in('m', 'f')),
per_dob date not null,
per_street varchar(30) not null,
per_city varchar(30) not null,
per_state char(2) not null default 'FL',
per_zip int not null check (per_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
per_email varchar(100) null,
per_type char(1) not null check (per_type in('c','s')),
per_notes varchar(45) null,
primary key(per_id),

constraint ux_per_ssn unique nonclustered (per_ssn asc)
);

if object_id (N'dbo.phone', N'U') is not null
drop table dbo.phone;
go

create table dbo.phone
(
phn_id smallint not null identity(1,1),
per_id smallint not null,
phn_num bigint not null check (phn_num like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
phn_type char(1) not null check (phn_type in ('h','c','w','f')),
phn_notes varchar(255) null,
primary key (phn_id),

constraint fk_phone_person
foreign key (per_id)
references dbo.person(per_id)
on delete cascade
on update cascade
);

if object_id (N'dbo.customer', N'U') is not null
drop table dbo.customer;
go

create table dbo.customer
(
per_id smallint not null,
cus_balance decimal(7,2) not null check (cus_balance >= 0),
cus_total_sales decimal(7,2) not null check (cus_total_sales >= 0),
cus_notes varchar(45) null,
primary key (per_id),

constraint fk_customer_person
foreign key (per_id)
references dbo.person (per_id)
on update cascade
on delete cascade
);

if object_id (N'dbo.slsrep', N'U') is not null
drop table dbo.slsrep;
go

create table dbo.slsrep
(
per_id smallint not null,
srp_yr_sales_goal decimal(8,2) not null check (srp_yr_sales_goal >= 0),
srp_ytd_sales decimal(8,2) not null check (srp_ytd_sales >= 0),
srp_ytd_comm decimal(7,2) not null check (srp_ytd_comm >= 0),
srp_notes varchar(45) null,
primary key (per_id),

constraint fk_slsrep_person
foreign key (per_id)
references dbo.person (per_id)
on update cascade
on delete cascade
);

if object_id (N'dbo.srp_hist', N'U') is not null
drop table dbo.srp_hist;
go

create table srp_hist
(
sht_id smallint not null identity(1,1),
per_id smallint not null,
sht_type char(1) not null check (sht_type in('i','u','d')),
sht_modified datetime not null,
sht_modifier varchar(45) not null default system_user,
sht_date date not null default getDate(),
sht_yr_sales_goal decimal(8,2) not null check (sht_yr_sales_goal >= 0),
sht_yr_total_sales decimal(8,2) not null check (sht_yr_total_sales >= 0),
sht_yr_total_comm decimal(7,2) not null check (sht_yr_total_comm >= 0),
sht_notes varchar(45) null,
primary key (sht_id),

constraint fk_srp_hist_slsrep
foreign key (per_id)
references dbo.slsrep (per_id)
on update cascade
on delete cascade
);

if object_id (N'dbo.contact', N'U') is not null
drop table dbo.contact;
go

create table contact
(
cnt_id int not null identity(1,1),
per_cid smallint not null,
per_sid smallint not null,
cnt_date datetime not null,
cnt_notes varchar(255) null,
primary key (cnt_id),

constraint fk_contact_customer
foreign key (per_cid)
references dbo.customer (per_id)
on update cascade
on delete cascade,

constraint fk_contact_slsrep
foreign key (per_sid)
references dbo.slsrep (per_id)
on update no action
on delete no action
);

if object_id (N'dbo.[order]', N'U') is not null
drop table dbo.[order];
go

create table [order]
(
ord_id int not null identity(1,1),
cnt_id int not null,
ord_placed_date datetime not null,
ord_filled_date datetime null,
ord_notes varchar(255) null,
primary key (ord_id),

constraint fk_order_contact
foreign key (cnt_id)
references dbo.contact (cnt_id)
on update cascade
on delete cascade
);

if object_id (N'dbo.store', N'U') is not null
drop table dbo.store;
go

create table store
(
str_id smallint not null identity(1,1),
str_name varchar(45) not null,
str_street varchar(30) not null,
str_city varchar(30) not null,
str_state char(2) not null default 'FL',
str_zip int not null check (str_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
str_phone bigint not null check (str_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
str_email varchar(100) not null,
str_url varchar(100) not null,
str_notes varchar(255) null,
primary key (str_id)
);

if object_id (N'dbo.invoice', N'U') is not null
drop table dbo.invoice;
go

create table invoice
(
inv_id int not null identity(1,1),
ord_id int not null,
str_id smallint not null,
inv_date datetime not null,
inv_total decimal(8,2) not null check (inv_total >= 0),
inv_paid bit not null,
inv_notes varchar(255) null,
primary key (inv_id),

constraint fk_invoice_order
foreign key (ord_id)
references dbo.[order] (ord_id)
on update cascade
on delete cascade,

constraint fk_invoice_store
foreign key (str_id)
references dbo.store (str_id)
on update cascade
on delete cascade
);

if object_id (N'dbo.payment', N'U') is not null
drop table dbo.payment;
go

create table payment
(
pay_id int not null identity(1,1),
inv_id int not null,
pay_date datetime not null,
pay_amt decimal(7,2) not null check (pay_amt >= 0),
pay_notes varchar(255) null,
primary key (pay_id),

constraint fk_payment_invoice
foreign key (inv_id)
references dbo.invoice (inv_id)
on update cascade
on delete cascade
);

if object_id (N'dbo.vendor', N'U') is not null
drop table dbo.vendor;
go

create table vendor
(
ven_id smallint not null identity(1,1),
ven_name varchar(45) not null,
ven_street varchar(30) not null,
ven_city varchar(30) not null,
ven_state char(2) not null default 'FL',
ven_zip int not null check (ven_zip like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
ven_phone bigint not null check (ven_phone like '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'),
ven_email varchar(100) not null,
ven_url varchar(100) not null,
ven_notes varchar(255) null,
primary key (ven_id)
);

if object_id (N'dbo.product', N'U') is not null
drop table dbo.product;
go

create table product
(
pro_id smallint not null identity(1,1),
ven_id smallint not null,
pro_name varchar(30) not null,
pro_descript varchar(45) null,
pro_weight float not null check (pro_weight >= 0),
pro_qoh smallint not null check (pro_qoh >= 0),
pro_cost decimal(7,2) not null check (pro_cost >= 0),
pro_price decimal(7,2) not null check (pro_price >= 0),
pro_discount decimal(3,0) null,
pro_notes varchar(255) null,
primary key (pro_id),

constraint fk_product_vendor
foreign key (ven_id)
references dbo.vendor (ven_id)
on update cascade
on delete cascade
);

if object_id (N'dbo.product_hist', N'U') is not null
drop table dbo.product_hist;
go

create table product_hist
(
pht_id int not null identity(1,1),
pro_id smallint not null,
pht_date datetime not null,
pht_cost decimal(7,2) not null check (pht_cost >= 0),
pht_price decimal(7,2) not null check (pht_price >= 0),
pht_discount decimal(3,0) null,
pht_notes varchar(255) null,
primary key (pht_id),

constraint fk_product_hist_product
foreign key (pro_id)
references dbo.product (pro_id)
on update cascade
on delete cascade
);

if object_id (N'dbo.order_line', N'U') is not null
drop table dbo.order_line;
go

create table order_line
(
oln_id int not null identity(1,1),
ord_id int not null,
pro_id smallint not null,
oln_qty smallint not null check (oln_qty >= 0),
oln_price decimal(7,2) not null check (oln_price >= 0),
oln_notes varchar(255) null,
primary key (oln_id),

constraint fk_order_line_order
foreign key (ord_id)
references dbo.[order] (ord_id)
on update cascade
on delete cascade,

constraint fk_order_line_product
foreign key (pro_id)
references dbo.product (pro_id)
on update cascade
on delete cascade
);

select * from information_schema.tables;

insert into dbo.person
(per_ssn, per_fname, per_lname, per_gender, per_dob, per_street, per_city, per_state, per_zip, per_email, per_type, per_notes)
values
(HASHBYTES('SHA2_512', '123456789'), 'John', 'Smith', 'm', '1990-05-02', '123 Now St', 'Fake City', 'FL', 123456789, 'johnsmith@gmail.com', 's', null),
(HASHBYTES('SHA2_512', '123456780'), 'Jack', 'Smith', 'm', '1992-02-22', '124 Now St', 'Fake City', 'FL', 123456789, 'jacksmith@gmail.com', 's', null),
(HASHBYTES('SHA2_512', '223456789'), 'Josh', 'Smith', 'm', '1999-08-12', '125 Now St', 'Fake City', 'FL', 123456789, 'joshsmith@gmail.com', 's', null),
(HASHBYTES('SHA2_512', '323456789'), 'Job', 'Smith', 'm', '2000-11-04', '126 Now St', 'Fake City', 'FL', 123456789, 'jobsmith@gmail.com', 's', null),
(HASHBYTES('SHA2_512', '423456789'), 'Johnny', 'Smith', 'm', '1995-07-15', '127 Now St', 'Fake City', 'FL', 123456789, 'johnnysmith@gmail.com', 's', null),
(HASHBYTES('SHA2_512', '523456789'), 'Jane', 'Smith', 'f', '1997-03-01', '128 Now St', 'Fake City', 'FL', 123456789, 'janesmith@gmail.com', 'c', null),
(HASHBYTES('SHA2_512', '623456789'), 'Jackie', 'Smith', 'f', '1999-06-30', '129 Now St', 'Fake City', 'FL', 123456789, 'jackiesmith@gmail.com', 'c', null),
(HASHBYTES('SHA2_512', '723456789'), 'Jessie', 'Smith', 'f', '1990-01-27', '130 Now St', 'Fake City', 'FL', 123456789, 'jessiesmith@gmail.com', 'c', null),
(HASHBYTES('SHA2_512', '823456789'), 'Jesse', 'Smith', 'm', '1993-12-18', '131 Now St', 'Fake City', 'FL', 123456789, 'jessesmith@gmail.com', 'c', null),
(HASHBYTES('SHA2_512', '923456789'), 'Julie', 'Smith', 'f', '1996-09-12', '132 Now St', 'Fake City', 'FL', 123456789, 'juliesmith@gmail.com', 'c', null);

insert into dbo.slsrep
(per_id, srp_yr_sales_goal, srp_ytd_sales, srp_ytd_comm, srp_notes)
values
(1, 100000, 50000, 1000, null),
(2, 200000, 60000, 2000, null),
(3, 300000, 70000, 3000, null),
(4, 400000, 80000, 4000, null),
(5, 500000, 90000, 5000, null);

insert into dbo.customer
(per_id, cus_balance, cus_total_sales, cus_notes)
values
(6, 100, 1000, null),
(7, 200, 2000, null),
(8, 300, 3000, null),
(9, 400, 4000, null),
(10, 500, 5000, null);

insert into dbo.contact
(per_sid, per_cid, cnt_date, cnt_notes)
values
(1, 6, 2020-01-10, null),
(2, 7, 2020-09-19, null),
(3, 8, 2020-03-09, null),
(4, 9, 2020-10-17, null),
(5, 10, 2020-12-20, null);

insert into dbo.[order]
(cnt_id, ord_placed_date, ord_filled_date, ord_notes)
values
(1, 2020-01-11, 2020-01-18, null),
(2, 2020-09-20, 2020-09-27, null),
(3, 2020-03-10, 2020-03-17, null),
(4, 2020-10-18, 2020-10-25, null),
(5, 2020-12-21, 2020-12-24, null);

insert into dbo.store
(str_name, str_street, str_city, str_state, str_zip, str_phone, str_email, str_url, str_notes)
values
('The Store', '123 Store Lane', 'Fake City', 'FL', 123456789, 1234567890, 'thestore@gmail.com', 'www.thestore.com', null),
('A Store', '124 Store Lane', 'Fake City', 'FL', 123456789, 1234567891, 'astore@gmail.com', 'www.astore.com', null),
('Da Store', '125 Store Lane', 'Fake City', 'FL', 123456789, 1234567892, 'dastore@gmail.com', 'www.dastore.com', null),
('Thy Store', '126 Store Lane', 'Fake City', 'FL', 123456789, 1234567893, 'thystore@gmail.com', 'www.thystore.com', null),
('Store', '123 Store Lane', 'Fake City', 'FL', 123456789, 1234567894, 'store@gmail.com', 'www.store.com', null);

insert into dbo.invoice
(ord_id, str_id, inv_date, inv_total, inv_paid, inv_notes)
values
(1, 1, 2020-01-11, 100, 1, null),
(2, 2, 2020-09-20, 200, 1, null),
(3, 3, 2020-03-10, 300, 1, null),
(4, 4, 2020-10-18, 400, 1, null),
(5, 5, 2020-12-21, 500, 1, null);

insert into dbo.vendor
(ven_name, ven_street, ven_city, ven_state, ven_zip, ven_phone, ven_email, ven_url, ven_notes)
values
('The Vendor', '123 Vendor Way', 'Fake City', 'FL', 123456789, 2123456789, 'thevendor@gmail.com', 'www.thevendor.com', null),
('A Vendor', '124 Vendor Way', 'Fake City', 'FL', 123456789, 3123456789, 'avendor@gmail.com', 'www.avendor.com', null),
('Da Vendor', '125 Vendor Way', 'Fake City', 'FL', 123456789, 4123456789, 'davendor@gmail.com', 'www.davendor.com', null),
('Thy Vendor', '126 Vendor Way', 'Fake City', 'FL', 123456789, 5123456789, 'thyvendor@gmail.com', 'www.thyvendor.com', null),
('Vendor', '127 Vendor Way', 'Fake City', 'FL', 123456789, 6123456789, 'vendor@gmail.com', 'www.vendor.com', null);

insert into dbo.product
(ven_id, pro_name, pro_descript, pro_weight, pro_qoh, pro_cost, pro_price, pro_discount, pro_notes)
values
(1, 'Product 1', '', 1.0, 50, 9.99, 12.99, null, null),
(2, 'Product 2', '', 2.0, 50, 6.99, 9.99, null, null),
(3, 'Product 3', '', 3.0, 50, 10.99, 13.99, null, null),
(4, 'Product 4', '', 4.0, 50, 23.99, 26.99, null, null),
(5, 'Product 5', '', 5.0, 50, 8.99, 11.99, null, null);

insert into dbo.order_line
(ord_id, pro_id, oln_qty, oln_price, oln_notes)
values
(1, 1, 1, 12.99, null),
(2, 2, 1, 9.99, null),
(3, 3, 1, 13.99, null),
(4, 4, 1, 26.99, null),
(5, 5, 1, 11.99, null);

insert into dbo.payment
(inv_id, pay_date, pay_amt, pay_notes)
values
(1, 2020-01-11, 12.99, null),
(2, 2020-09-20, 9.99, null),
(3, 2020-03-10, 13.99, null),
(4, 2020-10-18, 26.99, null),
(5, 2020-12-21, 11.99, null);

insert into dbo.product_hist
(pro_id, pht_date, pht_cost, pht_price, pht_discount, pht_notes)
values
(1, '2020-01-11 12:01:29', 9.99, 12.99, null, null),
(2, '2020-09-20 15:31:19', 6.99, 9.99, null, null),
(3, '2020-03-10 20:59:21', 10.99, 13.99, null, null),
(4, '2020-10-18 03:45:59', 23.99, 26.99, null, null),
(5, '2020-01-11 12:01:29', 8.99, 11.99, null, null);

insert into dbo.srp_hist
(per_id, sht_type, sht_modified, sht_modifier, sht_date, sht_yr_sales_goal, sht_yr_total_sales, sht_yr_total_comm, sht_notes)
values
(1, 'i', getDate(), SYSTEM_USER, getDate(), 100000, 50000, 1000, null),
(2, 'i', getDate(), SYSTEM_USER, getDate(), 200000, 60000, 2000, null),
(3, 'i', getDate(), SYSTEM_USER, getDate(), 300000, 70000, 3000, null),
(4, 'i', getDate(), SYSTEM_USER, getDate(), 400000, 80000, 4000, null),
(5, 'i', getDate(), SYSTEM_USER, getDate(), 500000, 90000, 5000, null);

insert into dbo.phone
(per_id, phn_num, phn_type, phn_notes)
values
(1, 1111111111, 'h', null),
(2, 2222222222, 'c', null),
(3, 3333333333, 'w', null),
(4, 4444444444, 'f', null),
(5, 5555555555, 'h', null);

create proc dbo.CreatePersonSSN
as
begin

declare @salt binary(64);
declare @ran_num int;
declare @ssn binary(64);
declare @x int, @y int;
set @x = 1;

set @y = (select count(*) from dbo.person);

while (@x > @y)
begin

set @salt = crypt_gen_random(64);
set @ran_num = floor(rand()*(999999999-111111111+1))+111111111;
set @ssn = hashbytes('SHA2_512', concat(@salt, @ran_num));

update dbo.person
set per_ssn = @ssn, per_salt = @salt
where per_id = @x;

set @x = @x + 1;

end;

end;
go

exec dbo.CreatePersonSSN