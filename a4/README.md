> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 

## Joshua Petree

### Assignment #4 Requirements:

*2 Parts*

1. Create and populate tables in MS SQL Server
2. Run specified queries based on exercises

#### README.md file should include the following items:

* Screenshot of ERD
* Selected screenshots of SQL code (optional)

#### Assignment Screenshots:

*Screenshot of ERD:

![A4 ERD Screenshot](img/lis3781_a4_erd.PNG)

*Screenshot of SQL Code (Part 1: Creating Tables)*:

![SQL Code Part 1 Screenshot](img/lis3781_a4_code_p1.PNG)

*Screenshot of SQL Code (Part 2: Populating Tables)*:

![SQL Code Part 2 Screenshot](img/lis3781_a4_code_p2.PNG)
