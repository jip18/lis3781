# LIS3781 - Advanced Database Management

## Joshua Petree

### Assignment #5 Requirements:

*2 Parts:*

1. Update current database structure with new tables and relationships
2. Run SQL queries based on different scenarios

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of SQL code (optional)

#### Assignment Screenshots:

*Screenshot of ERD*:

![A5 ERD](img/lis3781_a5_erd.PNG)

*Screenshot of SQL Code*:

![A5 SQL Code](img/lis3781_a5_sql_sc.PNG)

*Screenshot of SQL Code Part 2*:

![A5 SQL Code 2](img/lis3781_a5_sql_sc2.PNG)