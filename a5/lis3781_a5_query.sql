set ansi_warnings on;
go

use jip18;
go

IF OBJECT_ID (N'dbo.time', N'U') IS NOT NULL
DROP TABLE dbo.time;
go

CREATE TABLE dbo.time
(
tim_id int not null identity(1,1),
tim_yr smallint not null,
tim_qtr tinyint not null, -- 1 - 4
tim_month tinyint not null, -- 1 - 12
tim_week tinyint not null, -- 1 - 52
tim_day tinyint not null, -- 1 - 7
tim_time time not null,
tim_notes varchar(255) null,
primary key(tim_id)
);
go

IF OBJECT_ID (N'dbo.region', N'U') IS NOT NULL
DROP TABLE dbo.region;
go

CREATE TABLE dbo.region
(
reg_id tinyint not null identity(1,1),
reg_name char(1) not null, -- c, n, e, s, w
reg_notes varchar(255) null,
primary key(reg_id)
);
go

IF OBJECT_ID (N'dbo.state', N'U') IS NOT NULL
DROP TABLE dbo.state;
go

CREATE TABLE dbo.state
(
ste_id tinyint not null identity(1,1),
reg_id tinyint not null,
ste_name char(2) not null default 'FL',
ste_notes varchar(255) null,
primary key(ste_id),

constraint fk_state_region
foreign key (reg_id)
references dbo.region (reg_id)
on delete cascade
on update cascade
);
go

IF OBJECT_ID (N'dbo.city', N'U') IS NOT NULL
DROP TABLE dbo.city;
go

CREATE TABLE dbo.city
(
cty_id smallint not null identity(1,1),
ste_id tinyint not null,
cty_name varchar(30) not null,
cty_notes varchar(255) null,
primary key(cty_id),

constraint fk_city_state
foreign key (ste_id)
references dbo.state (ste_id)
on delete cascade
on update cascade
);
go

ALTER TABLE dbo.store
ADD cty_id smallint null;
go

ALTER TABLE dbo.store
ADD CONSTRAINT fk_store_city foreign key (cty_id) references dbo.city (cty_id) on delete cascade on update cascade;
go

IF OBJECT_ID (N'dbo.sale', N'U') IS NOT NULL
DROP TABLE dbo.sale;
go

CREATE TABLE dbo.sale
(
pro_id smallint not null,
str_id smallint not null,
cnt_id int not null,
tim_id int not null,
sal_qty smallint not null,
sal_price decimal(8,2) not null,
sal_total decimal(8,2) not null,
sal_notes varchar(255) null,
primary key(pro_id, str_id, cnt_id, tim_id),

constraint ux_pro_id_str_id_cnt_id_tim_id
unique nonclustered (pro_id asc, str_id asc, cnt_id asc, tim_id asc),

constraint fk_sale_product
foreign key (pro_id)
references dbo.product (pro_id)
on delete cascade
on update cascade,

constraint fk_sale_store
foreign key (str_id)
references dbo.store (str_id)
on delete cascade
on update cascade,

constraint fk_sale_contact
foreign key (cnt_id)
references dbo.contact (cnt_id)
on delete cascade
on update cascade,

constraint fk_sale_time
foreign key (tim_id)
references dbo.time (tim_id)
on delete cascade
on update cascade
);
go

insert into  dbo.region
(reg_name, reg_notes)
values
('c', null),
('n', null),
('e', null),
('s', null),
('w', null);
go

insert into dbo.state
(reg_id, ste_name, ste_notes)
values
(1, 'NE', null),
(2, 'ME', null),
(3, 'PA', null),
(4, 'FL', null),
(5, 'CA', null);
go

insert into dbo.city
(ste_id, cty_name, cty_notes)
values
(1, 'Lincoln', null),
(2, 'Augusta', null),
(3, 'Pittsburgh', null),
(4, 'Tallahassee', null),
(5, 'San Francisco', null);
go

insert into dbo.time
(tim_yr, tim_qtr, tim_month, tim_week, tim_day, tim_time, tim_notes)
values
(2015, 1, 1, 1, 1, '02:45:28', null),
(2016, 2, 4, 14, 2, '12:33:14', null),
(2017, 3, 7, 27, 3, '04:55:03', null),
(2018, 4, 10, 40, 4, '22:01:59', null),
(2019, 4, 10, 40, 5, '18:20:22', null);
go

insert into dbo.sale
(pro_id, str_id, cnt_id, tim_id, sal_qty, sal_price, sal_total, sal_notes)
values
(1, 1, 1, 1, 1, 12.99, 12.99, null),
(1, 2, 1, 1, 1, 12.99, 12.99, null),
(1, 3, 1, 1, 1, 12.99, 12.99, null),
(1, 4, 1, 1, 1, 12.99, 12.99, null),
(1, 5, 1, 1, 1, 12.99, 12.99, null),
(1, 1, 2, 1, 1, 12.99, 12.99, null),
(1, 1, 3, 1, 1, 12.99, 12.99, null),
(1, 1, 4, 1, 1, 12.99, 12.99, null),
(1, 1, 5, 1, 1, 12.99, 12.99, null),
(1, 1, 1, 2, 1, 12.99, 12.99, null),
(1, 1, 1, 3, 1, 12.99, 12.99, null),
(1, 1, 1, 4, 1, 12.99, 12.99, null),
(1, 1, 1, 5, 1, 12.99, 12.99, null),
(2, 1, 1, 1, 1, 9.99, 9.99, null),
(2, 2, 1, 1, 1, 9.99, 9.99, null),
(2, 3, 1, 1, 1, 9.99, 9.99, null),
(2, 4, 1, 1, 1, 9.99, 9.99, null),
(2, 5, 1, 1, 1, 9.99, 9.99, null),
(2, 1, 2, 1, 1, 9.99, 9.99, null),
(2, 1, 3, 1, 1, 9.99, 9.99, null),
(2, 1, 4, 1, 1, 9.99, 9.99, null),
(2, 1, 5, 1, 1, 9.99, 9.99, null),
(2, 1, 1, 2, 1, 9.99, 9.99, null),
(2, 1, 1, 3, 1, 9.99, 9.99, null),
(2, 1, 1, 4, 1, 9.99, 9.99, null);
go